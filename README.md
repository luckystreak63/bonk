# `bonk`

A 2D platforming physics for the [Amethyst](https://github.com/amethyst/amethyst) game engine. Uses [ultraviolet]() crate for math.

Add the following to `Cargo.toml`:
```toml
[dependencies]
bonk = "0.1.0"
ultraviolet = "0.5.0"
```

## Setup :
```rust
use bonk::BonkBundle;

let game_data = GameDataBuilder::default()
    // ...
    .with_bundle(BonkBundle)?
    .with_bundle(TransformBundle::new().with_dep(&["transform_update_system"]))?
    // ...
```

## Usage :
```rust
use ultraviolet::Vec2;
use bonk::{Collider, RigidBody};

world
    .create_entity()
    .with(Collider::new(Vec2::new(POS_X, POS_Y), Vec2::new(SIZE_X, SIZE_Y), is_opaque))
    //       ^---- Trigger collision events             Acts as a platform ------^
    .with(RigidBody::default())
    //        ^---- Add velocity for movement, configurable gravity
```
