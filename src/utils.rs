use std::ops::Sub;
use ultraviolet::Vec2;

pub trait EqualsEps {
    fn eq_eps(&self, other: Self, eps: f32) -> bool;
}

pub trait ToPercent {
    fn to_percent(&self) -> Self;
}

pub trait MaxForSign {
    fn max_for_sign_or_other(&self, other: Self) -> Self;
}

impl EqualsEps for f32 {
    fn eq_eps(&self, other: Self, eps: f32) -> bool {
        (self - other).abs() <= eps
    }
}

impl EqualsEps for Vec2 {
    fn eq_eps(&self, other: Self, eps: f32) -> bool {
        let diff = self.sub(other);
        let distance: f32 = (diff.x.powf(2.0) + diff.y.powf(2.0)).sqrt();
        distance <= eps
    }
}

impl ToPercent for Vec2 {
    fn to_percent(&self) -> Self {
        let total = self.x.abs() + self.y.abs();
        Vec2 {
            x: self.x / total,
            y: self.y / total,
        }
    }
}

impl MaxForSign for f32 {
    fn max_for_sign_or_other(&self, other: Self) -> Self {
        let same_sign = self.is_sign_positive() == other.is_sign_positive();
        if same_sign {
            return self.abs().max(other.abs()).copysign(other);
        }
        return other;
    }
}

#[cfg(test)]
mod tests {
    use crate::utils::{EqualsEps, MaxForSign, ToPercent};
    use ultraviolet::Vec2;

    #[test]
    fn to_percent() {
        let result = Vec2::new(30., 70.).to_percent();
        assert!(result.eq_eps(Vec2::new(0.3, 0.7), 0.01));
    }

    #[test]
    fn max_for_sign() {
        let x = 5.0;
        assert_eq!(x.max_for_sign_or_other(4.0), x);
        assert_eq!(x.max_for_sign_or_other(-4.0), -4.0);
        assert_eq!(x.max_for_sign_or_other(6.0), 6.0);
    }
}
