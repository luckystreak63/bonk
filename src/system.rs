use std::collections::{HashMap, HashSet};
use std::ops::{AddAssign, Mul, SubAssign};

use amethyst_core::ecs::prelude::{ComponentEvent, SystemData};
use amethyst_core::ecs::shrev::EventChannel;
use amethyst_core::ecs::{
    Entities, Entity, Join, Read, ReadStorage, ReaderId, System, World, WorldExt, Write,
    WriteStorage,
};
use amethyst_core::num::Zero;
use amethyst_core::{Axis2, SystemDesc, Transform};
use rstar::{RTree, RTreeObject};
use ultraviolet::Vec2;

use crate::collider::{Collider, FrameRate, Friction, Gravity, RTreeCollider, RigidBody};
use crate::event::{AxisResult, CollisionAxes, CollisionDetails};
use crate::CollisionEvent;

/// Applies gravity's force to a `RigidBody`s `velocity`.
/// The gravitational force is affected by the scale of time,
/// and also by the `RigidBody`s `gravity` multiplier.
#[derive(Default)]
pub struct GravitySystem;

impl<'s> System<'s> for GravitySystem {
    type SystemData = (
        WriteStorage<'s, RigidBody>,
        Read<'s, FrameRate>,
        Read<'s, Gravity>,
    );

    fn run(&mut self, (mut bodies, fps, gravity): Self::SystemData) {
        for body in (&mut bodies).join() {
            let gravity = body.gravity.mul(gravity.strength * fps.fps);
            body.local_velocity.add_assign(gravity);
        }
    }
}

#[derive(Default)]
pub struct RTreeChannel(Option<ReaderId<ComponentEvent>>);

/// Builds a `RTreeBookkeepingSystem`.
#[derive(Default, Debug)]
pub struct RTreeBookkeepingSystemDesc;

impl<'a, 'b> SystemDesc<'a, 'b, RTreeBookkeepingSystem> for RTreeBookkeepingSystemDesc {
    fn build(self, world: &mut World) -> RTreeBookkeepingSystem {
        <RTreeBookkeepingSystem as System<'_>>::SystemData::setup(world);

        let mut locals = WriteStorage::<Collider>::fetch(&world);
        let mut channel = world.write_resource::<RTreeChannel>();
        if channel.0.is_none() {
            let id = locals.register_reader();
            channel.0 = Some(id);
        };

        RTreeBookkeepingSystem::default()
    }
}

#[derive(Default)]
pub struct BookKeeping {
    cache: HashMap<u32, RTreeCollider>,
}

#[derive(Default)]
/// Inserts and removes Colliders from the RTree when a component is added or removed
pub struct RTreeBookkeepingSystem;

impl<'s> System<'s> for RTreeBookkeepingSystem {
    type SystemData = (
        ReadStorage<'s, Collider>,
        WriteStorage<'s, RTreeCollider>,
        Entities<'s>,
        Write<'s, RTree<RTreeCollider>>,
        Write<'s, BookKeeping>,
        Write<'s, RTreeChannel>,
    );

    fn run(
        &mut self,
        (colliders, mut rcolliders, entities, mut rtree, mut books, mut events): Self::SystemData,
    ) {
        let mut modified = HashSet::new();
        let mut removed = HashSet::new();

        colliders
            .channel()
            .read(events.0.as_mut().unwrap())
            .for_each(|event| match event {
                ComponentEvent::Inserted(id) | ComponentEvent::Modified(id) => {
                    modified.insert(*id);
                }
                ComponentEvent::Removed(id) => {
                    removed.insert(*id);
                }
            });

        for id in modified.iter() {
            let entity = entities.entity(*id);
            if let Some(rcol) = rcolliders.get(entity) {
                let _ = rtree.remove(rcol);
            }

            // Re-insert in correct spot
            if let Some(collider) = colliders.get(entity) {
                let rtree_collider = RTreeCollider::new(entity, collider);
                rtree.insert(rtree_collider.clone());
                books.cache.insert(entity.id(), rtree_collider.clone());
                match rcolliders.insert(entity, rtree_collider) {
                    Ok(_) => {}
                    Err(err) => eprintln!("Error inserting RTreeCollider component: {:?}", err),
                }
            } else {
                if !removed.contains(id) {
                    println!(
                        "Failed to find collider in storage for entity : {:?}",
                        entity
                    );
                }
            }
        }

        for id in &removed {
            let entity = entities.entity(*id);
            if let Some(rcol) = books.cache.get(id) {
                let has_col = rcolliders.get(entity).is_some();
                if entity.gen().id() == -1 || !has_col {
                    rtree.remove(rcol);
                    books.cache.remove(&id);
                }
            }
        }
    }
}

/// Stops velocity upon collision and fires events
#[derive(Default)]
pub struct CollisionSystem {
    previous_collisions: HashMap<(Entity, Entity), bool>,
    is_second_pass: bool,
}

impl CollisionSystem {
    pub fn new(is_second_pass: bool) -> Self {
        CollisionSystem {
            previous_collisions: HashMap::default(),
            is_second_pass,
        }
    }
}

impl<'s> System<'s> for CollisionSystem {
    type SystemData = (
        WriteStorage<'s, Collider>,
        WriteStorage<'s, RigidBody>,
        WriteStorage<'s, RTreeCollider>,
        Read<'s, FrameRate>,
        Write<'s, RTree<RTreeCollider>>,
        Write<'s, EventChannel<CollisionEvent>>,
        Entities<'s>,
        Write<'s, BookKeeping>,
    );

    fn run(
        &mut self,
        (mut colliders, mut bodies, mut r_cols, fps, mut rtree, mut channel, entities, mut books): Self::SystemData,
    ) {
        let mut events = Vec::new();
        for (collider, body, entity) in (&mut colliders, &mut bodies, &entities).join() {
            // Remove the current collider from the tree
            if let Some(rcol) = r_cols.get(entity) {
                rtree.remove(&rcol);
            }

            // Go through this twice
            // The first time will adjust velocity according to collision on one axis
            // The second time will adjust according to other axis
            let mut collision_entities = Vec::new();
            if body.is_kinematic {
                // Get all intersecting envelopes with future pos
                // Filter out non-opaque peeps
                let intersections = rtree.locate_in_envelope_intersecting(
                    &RTreeCollider::new(entity, collider)
                        .translated(body.project_move(fps.fps))
                        .envelope(),
                );
                let intersects: Vec<&RTreeCollider> = intersections
                    .into_iter()
                    .filter(|rtc| rtc.opaque || body.is_kinematic)
                    .collect();

                // Don't do anything if you were already colliding with them
                // This deals with the situation where a platform gets stuck inside you pushing you into a wall
                // Without this, the platform would carry you with it on its way out
                let intersections2 = rtree.locate_in_envelope_intersecting(
                    &RTreeCollider::new(entity, collider).envelope(),
                );
                let intersects2: Vec<&RTreeCollider> = intersections2
                    .into_iter()
                    .filter(|rtc| rtc.opaque || body.is_kinematic)
                    .collect();
                let intersects: Vec<&RTreeCollider> = intersects
                    .into_iter()
                    .filter(|x| !intersects2.contains(x))
                    .collect();

                let temp_rcol = RTreeCollider::new(entity, &collider);
                for other in &intersects {
                    if temp_rcol.collides_on_axis(
                        other,
                        Axis2::Y as usize,
                        &body.project_move(fps.fps),
                    ) {
                        let mut axe = CollisionAxes::default();
                        axe.y = match body.global_velocity.y > 0.0 {
                            true => AxisResult::Positive,
                            false => AxisResult::Negative,
                        };
                        if other.entity.id() == entity.id() {
                            continue;
                        }
                        collision_entities.push((
                            other.entity,
                            Some(false),
                            axe,
                            body.project_move(fps.fps),
                        ));
                    } else {
                        let mut axe = CollisionAxes::default();
                        axe.x = match body.global_velocity.x > 0f32 {
                            true => AxisResult::Positive,
                            false => AxisResult::Negative,
                        };
                        collision_entities.push((
                            other.entity,
                            None,
                            axe,
                            body.project_move(fps.fps),
                        ));
                    }
                }
            } else {
                for _ in 0..2 {
                    // Get all intersecting envelopes with future pos
                    // Filter out non-opaque peeps
                    let intersections = rtree.locate_in_envelope_intersecting(
                        &RTreeCollider::new(entity, collider)
                            .translated(body.project_move(fps.fps))
                            .envelope(),
                    );
                    let intersects: Vec<&RTreeCollider> = intersections
                        .into_iter()
                        .filter(|rtc| rtc.opaque || body.is_kinematic)
                        .collect();

                    // Find distance to nearest hor/ver colliders like before
                    // Adjust new velocity
                    let result = body.collide_with_nearest_axis(
                        &RTreeCollider::new(entity, &collider),
                        intersects,
                        fps.fps,
                    );
                    if let Some((entity, axis, old_vel)) = result {
                        let mut axes = CollisionAxes::default();
                        let collide_with_floor = match axis {
                            Axis2::X => {
                                axes.x = match old_vel.x.is_sign_positive() {
                                    true => AxisResult::Positive,
                                    false => AxisResult::Negative,
                                };
                                !body.gravity.x.is_zero()
                                    && body.gravity.x.is_sign_positive()
                                        == old_vel.x.is_sign_positive()
                            }
                            Axis2::Y => {
                                axes.y = match old_vel.y.is_sign_positive() {
                                    true => AxisResult::Positive,
                                    false => AxisResult::Negative,
                                };
                                !body.gravity.y.is_zero()
                                    && body.gravity.y.is_sign_positive()
                                        == old_vel.y.is_sign_positive()
                            }
                        };
                        let mut floor_result = match collide_with_floor {
                            true => Some(true),
                            false => None,
                        };
                        if body.is_kinematic {
                            match axes.y {
                                AxisResult::Positive => floor_result = Some(true),
                                _ => floor_result = None,
                            }
                        }
                        let distance = body.project_move(fps.fps);
                        collision_entities.push((entity, floor_result, axes, distance));

                        // If the entity is_kinematic, it shouldn't move due to collisions
                        if body.is_kinematic {
                            body.global_velocity = old_vel;
                            continue;
                        }

                        // Move according to collision
                        match axis {
                            Axis2::X => {
                                collider.local_position.x += body.global_velocity.x;
                                collider.global_position.x += body.global_velocity.x;
                                body.global_velocity.x = 0.0;
                                body.local_velocity.x = 0.0;
                            }
                            Axis2::Y => {
                                collider.local_position.y += body.global_velocity.y;
                                collider.global_position.y += body.global_velocity.y;
                                body.global_velocity.y = 0.0;
                                body.local_velocity.y = 0.0;
                            }
                        }
                    }
                }
            }

            // Get all intersecting envelopes with new future pos
            let intersections = rtree.locate_in_envelope_intersecting(
                &RTreeCollider::new(entity, collider)
                    .translated(body.project_move(fps.fps))
                    .envelope(),
            );
            let intersects: Vec<(Entity, Option<bool>, CollisionAxes, Vec2)> = intersections
                .into_iter()
                .map(|col| (col.entity, None, CollisionAxes::default(), Vec2::default()))
                .collect();

            let normalize = |ent1: Entity, ent2: Entity| match ent1.id() < ent2.id() {
                true => (ent1, ent2),
                false => (ent2, ent1),
            };

            // Create collision events
            for (col, grav, axes, distance) in collision_entities.iter().chain(intersects.iter()) {
                if entity == *col {
                    continue;
                }
                let previous = self.previous_collisions.get(&normalize(entity, *col));
                if let Some(prev) = previous {
                    if *prev {
                        continue;
                    }
                }
                let event = CollisionEvent::enter_or_stay(
                    entity,
                    col,
                    *grav,
                    *axes,
                    *distance,
                    previous.is_some(),
                );
                events.push(event);
                self.previous_collisions
                    .insert(normalize(entity, *col), true);
            }

            // Re-insert in correct spot
            let rtree_collider =
                RTreeCollider::new(entity, collider).translated(body.project_move(fps.fps));
            rtree.insert(rtree_collider.clone());
            books.cache.insert(entity.id(), rtree_collider.clone());
            match r_cols.insert(entity, rtree_collider) {
                Ok(_) => {}
                Err(err) => eprintln!("Error inserting RTreeCollider component: {:?}", err),
            }
        }
        if !self.is_second_pass {
            // Calculate exit events
            for (entities, col) in self.previous_collisions.iter() {
                if !*col {
                    let end = CollisionEvent::OnCollisionExit(CollisionDetails::new(
                        entities.0,
                        entities.1,
                        None,
                        CollisionAxes::default(),
                        Vec2::default(),
                    ));
                    events.push(end);
                }
            }

            self.previous_collisions = self
                .previous_collisions
                .iter()
                .filter(|(_, b)| **b)
                .map(|(e, _)| ((e.0, e.1), false))
                .collect();
        }
        channel.iter_write(events);
    }
}

/// Slows a `RigidBody` if running on the ground.
pub struct FrictionSystem {
    reader_id: ReaderId<CollisionEvent>,
}

impl FrictionSystem {
    pub fn new(world: &mut World) -> Self {
        if world.try_fetch::<EventChannel<CollisionEvent>>().is_none() {
            world.insert(EventChannel::<CollisionEvent>::new());
        }
        let reader_id = world
            .fetch_mut::<EventChannel<CollisionEvent>>()
            .register_reader();
        Self { reader_id }
    }
}

impl<'s> System<'s> for FrictionSystem {
    type SystemData = (
        WriteStorage<'s, RigidBody>,
        ReadStorage<'s, Friction>,
        Read<'s, EventChannel<CollisionEvent>>,
        Read<'s, FrameRate>,
    );

    fn run(&mut self, (mut bodies, frictions, events, fps): Self::SystemData) {
        for event in events.read(&mut self.reader_id) {
            let details = event.details();

            if details.collide_with_floor.is_some() {
                let entities = vec![details.collider, details.collidee];
                for e in entities {
                    if let Some(body) = bodies.get_mut(e) {
                        let slow_amount = frictions
                            .get(details.collidee)
                            .unwrap_or(&Friction::default())
                            .slow_per_second;

                        // Slow amount
                        let mut slow = Vec2 {
                            x: slow_amount.copysign(body.local_velocity.x) * fps.fps,
                            y: slow_amount.copysign(body.local_velocity.y) * fps.fps,
                        };

                        // Don't over-slow so much that it moves in the opposite direction
                        if slow.x.abs() >= body.local_velocity.x.abs() {
                            slow.x = body.local_velocity.x;
                        }
                        if slow.y.abs() >= body.local_velocity.y.abs() {
                            slow.y = body.local_velocity.y;
                        }

                        // Gravity axis will be 0.0, other will be 1.0
                        let grav = Vec2 {
                            x: body.gravity.y.abs(),
                            y: body.gravity.x.abs(),
                        };

                        // We slow the move speed on the axis that isn't affected by gravity
                        body.local_velocity.sub_assign(slow.mul(grav));
                    }
                }
            }
        }
    }
}

/// Moves a `Collider` according to the `RigidBody` velocity.
/// All collision verification should already be complete by now.
pub struct RigidBodyMovementSystem;

impl<'s> System<'s> for RigidBodyMovementSystem {
    type SystemData = (
        WriteStorage<'s, RigidBody>,
        WriteStorage<'s, Collider>,
        Read<'s, FrameRate>,
    );

    fn run(&mut self, (mut bodies, mut colliders, fps): Self::SystemData) {
        for (body, collider) in (&mut bodies, &mut colliders).join() {
            let projection = body.project_move(fps.fps);
            collider.local_position.add_assign(projection);
            collider.global_position.add_assign(projection);
        }
    }
}

/// Synchronizes transform with `Collider` position.
#[derive(Default)]
pub struct TransformUpdateSystem;

impl<'s> System<'s> for TransformUpdateSystem {
    type SystemData = (ReadStorage<'s, Collider>, WriteStorage<'s, Transform>);

    fn run(&mut self, (colliders, mut transforms): Self::SystemData) {
        for (collider, transform) in (&colliders, &mut transforms).join() {
            transform.set_translation_x(collider.global_position.x);
            transform.set_translation_y(collider.global_position.y);
        }
    }
}

#[cfg(test)]
mod tests {
    use amethyst_core::ecs::shrev::EventChannel;
    use amethyst_core::ecs::{Builder, Entity, RunNow, World, WorldExt};
    use amethyst_core::SystemDesc;
    use ultraviolet::Vec2;

    use crate::parent::PositionParentSystem;
    use crate::system::{
        CollisionSystem, RTreeBookkeepingSystem, RTreeBookkeepingSystemDesc,
        RigidBodyMovementSystem,
    };
    use crate::utils::EqualsEps;
    use crate::{Collider, CollisionEvent, FrameRate, RigidBody};

    const EPS: f32 = 0.01f32;

    fn collision_world() -> (
        World,
        RTreeBookkeepingSystem,
        CollisionSystem,
        RigidBodyMovementSystem,
        PositionParentSystem,
    ) {
        let mut world = World::new();
        let mut rt = RTreeBookkeepingSystemDesc::default().build(&mut world);
        let mut cs = CollisionSystem::default();
        let mut rm = RigidBodyMovementSystem;
        let mut pp = PositionParentSystem;
        rt.setup(&mut world);
        cs.setup(&mut world);
        rm.setup(&mut world);
        pp.setup(&mut world);

        (world, rt, cs, rm, pp)
    }

    fn example_collision_setup(world: &mut World) -> (Entity, Entity, Entity) {
        let move_collider = Collider {
            global_position: Vec2::default(),
            local_position: Vec2::default(),
            lower: Vec2::new(0., 0.),
            upper: Vec2::new(1.0, 1.0),
            opaque: false,
        };
        let move_body = RigidBody {
            global_velocity: Vec2::new(1.75, 1.75),
            local_velocity: Vec2::default(),
            gravity: Vec2::default(),
            is_kinematic: false,
        };

        let obstacle_1 = Collider {
            global_position: Vec2::new(1.5, 1.0),
            local_position: Vec2::new(1.5, 1.0),
            lower: Vec2::new(0., 0.),
            upper: Vec2::new(1.0, 1.0),
            opaque: true,
        };
        let obstacle_2 = Collider {
            global_position: Vec2::new(0.75, 2.0),
            local_position: Vec2::new(0.75, 2.0),
            lower: Vec2::new(0., 0.),
            upper: Vec2::new(1.0, 1.0),
            opaque: true,
        };

        world.register::<Collider>();
        world.register::<RigidBody>();
        let moving_entity = world
            .create_entity()
            .with(move_collider.clone())
            .with(move_body.clone())
            .build();
        let obstacle1 = world.create_entity().with(obstacle_1.clone()).build();
        let obstacle2 = world.create_entity().with(obstacle_2.clone()).build();

        (moving_entity, obstacle1, obstacle2)
    }

    #[test]
    fn events() {
        let (mut world, mut rtree, mut coll, mut rigid, mut pp) = collision_world();
        let mut reader = world
            .write_resource::<EventChannel<CollisionEvent>>()
            .register_reader();

        let (moving_entity, obstacle1, obstacle2) = example_collision_setup(&mut world);

        let mut calculate_next_frame = |world: &mut World| {
            world.write_resource::<FrameRate>().fps = 1.0;
            rtree.run_now(&world);
            coll.run_now(&world);
            rigid.run_now(&world);
            pp.run_now(&world);
            world.maintain();
        };

        calculate_next_frame(&mut world);

        let move_collider = world
            .read_storage::<Collider>()
            .get(moving_entity)
            .unwrap()
            .clone();
        assert!(move_collider.global_position.x.eq_eps(0.5, EPS));
        assert!(move_collider.global_position.y.eq_eps(1.0, EPS));

        // CollisionEnter events were fired
        {
            let resource = world.read_resource::<EventChannel<CollisionEvent>>();
            let mut iterator = resource.read(&mut reader);
            let col_details = iterator.next().unwrap().details();
            assert_eq!(col_details.collider, moving_entity);
            assert_eq!(col_details.collidee, obstacle1);

            let col_details = iterator.next().unwrap().details();
            assert_eq!(col_details.collider, moving_entity);
            assert_eq!(col_details.collidee, obstacle2);

            assert!(iterator.next().is_none());
        }

        world
            .write_storage::<RigidBody>()
            .get_mut(moving_entity)
            .unwrap()
            .global_velocity = Vec2::new(0.5, 0.5);

        calculate_next_frame(&mut world);

        // CollisionStay events were fired
        {
            let resource = world.read_resource::<EventChannel<CollisionEvent>>();
            let mut iterator = resource.read(&mut reader);

            for _ in 0..2 {
                let event = iterator.next();
                if let Some(event) = event {
                    let is_stay_event = match event {
                        CollisionEvent::OnCollisionStay(_) => true,
                        _ => false,
                    };
                    assert!(is_stay_event);
                }
            }
            assert!(iterator.next().is_none());
        }

        calculate_next_frame(&mut world);

        // CollisionExit events were fired
        {
            let resource = world.read_resource::<EventChannel<CollisionEvent>>();
            let mut iterator = resource.read(&mut reader);

            for _ in 0..2 {
                let event = iterator.next();
                if let Some(event) = event {
                    let is_exit_event = match event {
                        CollisionEvent::OnCollisionExit(_) => true,
                        _ => false,
                    };
                    assert!(is_exit_event);
                }
            }
            assert!(iterator.next().is_none());
        }

        calculate_next_frame(&mut world);

        // No other events happen
        {
            let resource = world.read_resource::<EventChannel<CollisionEvent>>();
            let mut iterator = resource.read(&mut reader);

            assert!(iterator.next().is_none());
        }
    }
}
