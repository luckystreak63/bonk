use crate::collider::RTreeCollider;
use crate::Collider;
use amethyst_core::ecs::{Entity, World, WorldExt};
use rstar::{RTree, RTreeObject};
use ultraviolet::Vec2;

pub type CollisionTree = RTree<RTreeCollider>;

pub fn is_collider_at_point(point: Vec2, world: &mut World) -> bool {
    let rtree = world.read_resource::<RTree<RTreeCollider>>();
    rtree
        .locate_in_envelope_intersecting(&rstar::AABB::from_point([point.x, point.y]))
        .count()
        > 0
}

pub fn would_collide_opaque(entity: &Entity, collider: &Collider, rtree: &CollisionTree) -> bool {
    let aabb = RTreeCollider::new(entity.clone(), collider);
    let results = rtree.locate_in_envelope_intersecting(&aabb.envelope());
    for candidate in results {
        if candidate.opaque {
            return true;
        }
    }
    return false;
}
