use amethyst_core::ecs::Entity;
use derivative::Derivative;
use ultraviolet::Vec2;

/// Indicates the result of a collision.
///
/// `OnCollisionEnter` happens on the first frame of a collision.
/// `OnCollisionStay` happens on every subsequent frame of collision.
/// `OnCollisionExit` happens on the first frame that a collision ends.
///
/// Only entities with RigidBodies can fire these events.
/// The first entity will always be the moving entity currently being evaluated.
///
/// If both entities have `RigidBody`s, it is likely that the same event be fired twice
/// with the entities in both positions. For this reason, it is suggested to handle
/// collision events only for one permutation when possible, or keep a cache to prevent
/// code for the same collision to run twice.
#[derive(Debug, Copy, Clone)]
pub enum CollisionEvent {
    OnCollisionEnter(CollisionDetails),
    OnCollisionStay(CollisionDetails),
    OnCollisionExit(CollisionDetails),
}

impl CollisionEvent {
    pub(crate) fn enter_or_stay(
        current: Entity,
        collision: &Entity,
        collide_with_floor: Option<bool>,
        collision_axes: CollisionAxes,
        distance: Vec2,
        has_prev_collision: bool,
    ) -> Self {
        if has_prev_collision {
            return CollisionEvent::OnCollisionStay(CollisionDetails::new(
                current,
                *collision,
                collide_with_floor,
                collision_axes,
                distance,
            ));
        }
        CollisionEvent::OnCollisionEnter(CollisionDetails::new(
            current,
            *collision,
            collide_with_floor,
            collision_axes,
            distance,
        ))
    }

    /// Returns the inner entities of the event regardless of discriminant.
    pub fn details(&self) -> CollisionDetails {
        match self {
            CollisionEvent::OnCollisionEnter(det) => *det,
            CollisionEvent::OnCollisionStay(det) => *det,
            CollisionEvent::OnCollisionExit(det) => *det,
        }
    }
}

#[derive(Copy, Clone, Debug, Derivative, PartialEq)]
#[derivative(Default)]
pub enum AxisResult {
    Positive,
    Negative,
    #[derivative(Default)]
    None,
}

#[derive(Copy, Clone, Debug, Default)]
pub struct CollisionAxes {
    pub x: AxisResult,
    pub y: AxisResult,
}

#[derive(Copy, Clone, Debug)]
pub struct CollisionDetails {
    pub collider: Entity,
    pub collidee: Entity,
    pub collide_with_floor: Option<bool>,
    pub axes: CollisionAxes,
    pub distance: Vec2,
}

impl CollisionDetails {
    pub fn new(
        collider: Entity,
        collidee: Entity,
        collide_with_floor: Option<bool>,
        axes: CollisionAxes,
        distance: Vec2,
    ) -> CollisionDetails {
        CollisionDetails {
            collider,
            collidee,
            collide_with_floor,
            axes,
            distance,
        }
    }
}
