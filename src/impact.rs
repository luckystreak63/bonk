use std::ops::{AddAssign, SubAssign};

use amethyst_core::ecs::shrev::EventChannel;
use amethyst_core::ecs::{Entities, Join, Read, ReadStorage, System, World, WriteStorage};
use amethyst_core::shrev::ReaderId;
use ultraviolet::Vec2;

use crate::event::AxisResult;
use crate::parent::VelocityParent;
use crate::utils::{EqualsEps, MaxForSign};
use crate::{Collider, CollisionEvent, RigidBody};

/// Applies impact velocity.
pub struct ImpactSystem {
    reader_id: ReaderId<CollisionEvent>,
}

impl ImpactSystem {
    pub fn new(world: &mut World) -> Self {
        if world.try_fetch::<EventChannel<CollisionEvent>>().is_none() {
            world.insert(EventChannel::<CollisionEvent>::new());
        }
        let reader_id = world
            .fetch_mut::<EventChannel<CollisionEvent>>()
            .register_reader();
        Self { reader_id }
    }
}

impl<'s> System<'s> for ImpactSystem {
    type SystemData = (
        WriteStorage<'s, RigidBody>,
        WriteStorage<'s, VelocityParent>,
        ReadStorage<'s, Collider>,
        Entities<'s>,
        Read<'s, EventChannel<CollisionEvent>>,
    );

    fn run(
        &mut self,
        (mut bodies, mut vel_parents, colliders, entities, events): Self::SystemData,
    ) {
        let mut collision_events = Vec::new();
        for event in events.read(&mut self.reader_id) {
            collision_events.push(event.clone());
        }

        for (_collider, entity) in (&colliders, &entities).join() {
            let (is_kinematic, global_velocity) = {
                if let Some(body) = bodies.get(entity) {
                    (body.is_kinematic, body.global_velocity)
                } else {
                    (false, Vec2::default())
                }
            };
            if is_kinematic {
                let iterator = collision_events
                    .iter()
                    .filter(|x| x.details().collider == entity || x.details().collidee == entity);
                for ev in iterator {
                    let det = ev.details();

                    let other = match det.collidee == entity {
                        true => det.collider,
                        false => det.collidee,
                    };

                    if let Some(body) = bodies.get_mut(other) {
                        if body.is_kinematic {
                            continue;
                        }

                        let push_by_platform = |body: &mut RigidBody| {
                            let mut ignore = false;
                            if let Some(parent) = vel_parents.get(other) {
                                if parent.entity == entity {
                                    ignore = true;
                                }
                            }
                            if !ignore {
                                match det.axes.y {
                                    AxisResult::None => {}
                                    _ => {
                                        // println!("Pushed by platform!");
                                        body.local_velocity.y = body
                                            .local_velocity
                                            .y
                                            .max_for_sign_or_other(global_velocity.y);
                                        body.global_velocity.y = body
                                            .global_velocity
                                            .y
                                            .max_for_sign_or_other(global_velocity.y);
                                    }
                                }

                                let already_stopped = entity == det.collidee
                                    && body.local_velocity.x.eq_eps(0.0, 0.01);
                                let mut apply_push = true;
                                match det.axes.x {
                                    AxisResult::None => apply_push = false,
                                    AxisResult::Positive => {
                                        if already_stopped && global_velocity.x.is_sign_positive() {
                                            apply_push = false;
                                        }
                                    }
                                    AxisResult::Negative => {
                                        if already_stopped && global_velocity.x.is_sign_negative() {
                                            apply_push = false;
                                        }
                                    }
                                }
                                if apply_push {
                                    // println!(
                                    //     "Pushed by platform L{:?} - G{:?}",
                                    //     body.local_velocity.x, global_velocity.x
                                    // );
                                    body.local_velocity.x = body
                                        .local_velocity
                                        .x
                                        .max_for_sign_or_other(global_velocity.x);
                                    body.global_velocity.x = body
                                        .global_velocity
                                        .x
                                        .max_for_sign_or_other(global_velocity.x);
                                }
                            }
                        };

                        let collides_with_floor = match det.collide_with_floor {
                            Some(x) => {
                                if x {
                                    true
                                } else {
                                    if global_velocity.y.is_sign_positive()
                                        != body.gravity.y.is_sign_positive()
                                        && det.collider == entity
                                    {
                                        true
                                    } else {
                                        false
                                    }
                                }
                            }
                            None => false,
                        };

                        match &ev {
                            CollisionEvent::OnCollisionEnter(_) => {
                                // print!("Enter: ");
                                if collides_with_floor {
                                    // println!("Parenting!");
                                    let _ = vel_parents.insert(other, VelocityParent { entity });
                                    body.local_velocity.x.sub_assign(global_velocity.x);
                                    body.global_velocity.x.sub_assign(global_velocity.x);
                                    let mut value = 0.0;
                                    match det.axes.y {
                                        AxisResult::Negative => {
                                            // Set the new vel
                                            value = -det.distance.y;
                                        }
                                        _ => {}
                                    }
                                    body.global_velocity.y = 0.0;
                                    body.local_velocity.y = value;
                                // body.local_velocity.y = distance.y;
                                } else {
                                    push_by_platform(body);
                                }
                            }
                            CollisionEvent::OnCollisionStay(_) => {
                                if collides_with_floor && body.local_velocity.y.is_sign_negative() {
                                    body.global_velocity.y = 0.0;
                                    body.local_velocity.y = 0.0;
                                }
                                // print!("Stay: ");
                                push_by_platform(body);
                            }
                            CollisionEvent::OnCollisionExit(_) => {
                                // print!("Exit: ");
                                let remove = {
                                    let mut result = false;
                                    if let Some(parent) = vel_parents.get(other) {
                                        if parent.entity == entity {
                                            result = true;
                                        }
                                    }
                                    result
                                };
                                if remove {
                                    // println!("Un-parenting!");
                                    vel_parents.remove(other);
                                    body.local_velocity.add_assign(global_velocity);
                                    body.global_velocity.add_assign(global_velocity);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use amethyst_core::ecs::{Builder, Entity, RunNow, World, WorldExt};
    use amethyst_core::SystemDesc;
    use ultraviolet::Vec2;

    use crate::impact::ImpactSystem;
    use crate::parent::{PositionParentSystem, VelocityParentSystem};
    use crate::system::{
        CollisionSystem, RTreeBookkeepingSystem, RTreeBookkeepingSystemDesc,
        RigidBodyMovementSystem,
    };
    use crate::utils::EqualsEps;
    use crate::{Collider, FrameRate, RigidBody};

    const EPS: f32 = 0.01f32;

    fn collision_world() -> (
        World,
        RTreeBookkeepingSystem,
        VelocityParentSystem,
        CollisionSystem,
        ImpactSystem,
        RigidBodyMovementSystem,
        PositionParentSystem,
    ) {
        let mut world = World::new();
        let mut rt = RTreeBookkeepingSystemDesc::default().build(&mut world);
        let mut vp = VelocityParentSystem;
        let mut pp = PositionParentSystem;
        let mut cs = CollisionSystem::default();
        let mut is = ImpactSystem::new(&mut world);
        let mut rm = RigidBodyMovementSystem;
        rt.setup(&mut world);
        vp.setup(&mut world);
        pp.setup(&mut world);
        cs.setup(&mut world);
        is.setup(&mut world);
        rm.setup(&mut world);

        (world, rt, vp, cs, is, rm, pp)
    }

    fn example_collision_setup(world: &mut World) -> (Entity, Entity) {
        let left_collider = Collider {
            global_position: Vec2::default(),
            local_position: Vec2::default(),
            lower: Vec2::new(0., 0.),
            upper: Vec2::new(1.0, 1.0),
            opaque: false,
        };
        let left_body = RigidBody {
            global_velocity: Vec2::new(1.75, 0.01),
            local_velocity: Vec2::new(1.75, 0.01),
            gravity: Vec2::default(),
            is_kinematic: false,
        };

        let right_collider = Collider {
            global_position: Vec2::new(1.5, 0.0),
            local_position: Vec2::new(1.5, 0.0),
            lower: Vec2::new(0., 0.),
            upper: Vec2::new(1.0, 1.0),
            opaque: true,
        };
        let right_body = RigidBody {
            global_velocity: Vec2::new(0.1, 0.),
            local_velocity: Vec2::new(0.1, 0.),
            gravity: Vec2::default(),
            is_kinematic: true,
        };

        world.register::<Collider>();
        world.register::<RigidBody>();
        let left_entity = world
            .create_entity()
            .with(left_collider.clone())
            .with(left_body.clone())
            .build();
        let right_entity = world
            .create_entity()
            .with(right_collider.clone())
            .with(right_body.clone())
            .build();

        (left_entity, right_entity)
    }

    #[test]
    fn impact() {
        let (mut world, mut rtree, mut vp, mut coll, mut is, mut rigid, mut pp) = collision_world();

        let (left_ent, right_ent) = example_collision_setup(&mut world);

        let mut calculate_next_frame = |world: &mut World| {
            world.write_resource::<FrameRate>().fps = 1.0;
            rtree.run_now(&world);
            vp.run_now(&world);
            coll.run_now(&world);
            is.run_now(&world);
            vp.run_now(&world);
            coll.run_now(&world);
            rigid.run_now(&world);
            pp.run_now(&world);
            world.maintain();
        };

        calculate_next_frame(&mut world);

        let left_collider = world
            .read_storage::<Collider>()
            .get(left_ent)
            .unwrap()
            .clone();
        assert!(left_collider.global_position.x.eq_eps(0.5, EPS));
        assert!(left_collider.global_position.y.eq_eps(0.01, EPS));

        let new_left_speed = -0.5;
        world
            .write_storage::<RigidBody>()
            .get_mut(right_ent)
            .unwrap()
            .local_velocity
            .x = new_left_speed;

        calculate_next_frame(&mut world);

        let vel = world
            .read_storage::<RigidBody>()
            .get(left_ent)
            .unwrap()
            .global_velocity
            .clone();
        assert_eq!(vel.x, new_left_speed);

        let left_collider = world
            .read_storage::<Collider>()
            .get(left_ent)
            .unwrap()
            .clone();
        assert!(left_collider.global_position.x.eq_eps(0.0, EPS));
        assert!(left_collider.global_position.y.eq_eps(0.02, EPS));
        // let parent = world.read_storage::<VelocityParent>().get(left_ent).unwrap().entity.clone();
        // assert_eq!(parent, right_ent);
    }
}
