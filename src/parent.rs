use crate::{Collider, RigidBody};
use amethyst_core::ecs::{
    Component, DenseVecStorage, Entities, Entity, Join, ReadStorage, System, WriteStorage,
};
use derivative::Derivative;
use std::ops::Add;
use ultraviolet::Vec2;

#[derive(Derivative, Component)]
pub struct PositionParent {
    pub entity: Entity,
}
impl PositionParent {
    pub fn new(entity: Entity) -> Self {
        PositionParent { entity }
    }
}

#[derive(Default)]
pub struct PositionParentSystem;

impl<'s> System<'s> for PositionParentSystem {
    type SystemData = (
        WriteStorage<'s, Collider>,
        ReadStorage<'s, PositionParent>,
        Entities<'s>,
    );

    fn run(&mut self, (mut colliders, parents, entities): Self::SystemData) {
        // Hack to be able to recursively call a closure
        struct CopyPosition<'s> {
            f: &'s dyn Fn(&CopyPosition, Entity, &mut WriteStorage<Collider>) -> Vec2,
        }
        let copy_position = CopyPosition {
            f: &|copy_position, target: Entity, mut colliders| -> Vec2 {
                let mut position = Vec2::default();
                let mut parent = None;
                if let Some(collider) = colliders.get_mut(target) {
                    position = collider.local_position;
                }
                if let Some(position_parent) = parents.get(target) {
                    parent = Some(position_parent.entity);
                }
                if parent.is_some() {
                    position = position.add((copy_position.f)(
                        copy_position,
                        parent.unwrap(),
                        &mut colliders,
                    ));
                }
                if let Some(mut coll) = colliders.get_mut(target) {
                    coll.global_position = position;
                }
                return position;
            },
        };

        for entity in (&entities).join() {
            (copy_position.f)(&copy_position, entity, &mut colliders);
        }
    }
}

#[derive(Derivative, Component)]
pub struct VelocityParent {
    pub entity: Entity,
}

#[derive(Default)]
pub struct VelocityParentSystem;

impl<'s> System<'s> for VelocityParentSystem {
    type SystemData = (
        WriteStorage<'s, RigidBody>,
        ReadStorage<'s, VelocityParent>,
        Entities<'s>,
    );

    fn run(&mut self, (mut bodies, parents, entities): Self::SystemData) {
        // Hack to be able to recursively call a closure
        struct UpToSpeed<'s> {
            f: &'s dyn Fn(&UpToSpeed, Entity, &mut WriteStorage<RigidBody>) -> Vec2,
        }
        let up_to_speed = UpToSpeed {
            f: &|up_to_speed, target: Entity, mut bodies| -> Vec2 {
                let mut velocity = Vec2::default();
                let mut parent = None;
                if let Some(body) = bodies.get_mut(target) {
                    velocity = body.local_velocity;
                }
                if let Some(velocity_parent) = parents.get(target) {
                    parent = Some(velocity_parent.entity);
                }
                if parent.is_some() {
                    velocity =
                        velocity.add((up_to_speed.f)(up_to_speed, parent.unwrap(), &mut bodies));
                }
                if let Some(mut body) = bodies.get_mut(target) {
                    body.global_velocity = velocity;
                }
                return velocity;
            },
        };

        for entity in (&entities).join() {
            (up_to_speed.f)(&up_to_speed, entity, &mut bodies);
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::parent::{
        PositionParent, PositionParentSystem, VelocityParent, VelocityParentSystem,
    };
    use crate::system::{
        CollisionSystem, RTreeBookkeepingSystem, RTreeBookkeepingSystemDesc,
        RigidBodyMovementSystem,
    };
    use crate::utils::EqualsEps;
    use crate::{Collider, FrameRate, RigidBody};
    use amethyst_core::ecs::{Builder, Entity, RunNow, World, WorldExt};
    use amethyst_core::SystemDesc;
    use ultraviolet::Vec2;

    const EPS: f32 = 0.01f32;

    fn collision_world() -> (
        World,
        RTreeBookkeepingSystem,
        PositionParentSystem,
        VelocityParentSystem,
        CollisionSystem,
        RigidBodyMovementSystem,
    ) {
        let mut world = World::new();
        let mut rt = RTreeBookkeepingSystemDesc::default().build(&mut world);
        let mut pp = PositionParentSystem::default();
        let mut vp = VelocityParentSystem::default();
        let mut cs = CollisionSystem::default();
        let mut rm = RigidBodyMovementSystem;
        rt.setup(&mut world);
        pp.setup(&mut world);
        vp.setup(&mut world);
        cs.setup(&mut world);
        rm.setup(&mut world);

        (world, rt, pp, vp, cs, rm)
    }

    fn example_parenting_setup(world: &mut World) -> (Entity, Entity) {
        let move_collider = Collider {
            global_position: Vec2::default(),
            local_position: Vec2::default(),
            lower: Vec2::new(-1., -1.),
            upper: Vec2::new(1.0, 1.0),
            opaque: true,
        };
        let move_body = RigidBody {
            local_velocity: Vec2::new(0.0, -1.0),
            global_velocity: Vec2::default(),
            gravity: Vec2::default(),
            is_kinematic: false,
        };

        let platform_collider = Collider {
            global_position: Vec2::new(0.0, -2.001),
            local_position: Vec2::default(),
            lower: Vec2::new(-1., -1.),
            upper: Vec2::new(1.0, 1.0),
            opaque: true,
        };
        let platform_body = RigidBody {
            local_velocity: Vec2::new(0.5, 0.0),
            global_velocity: Vec2::default(),
            gravity: Vec2::default(),
            is_kinematic: true,
        };

        let ship_body = RigidBody {
            local_velocity: Vec2::new(0.5, 0.0),
            global_velocity: Vec2::default(),
            gravity: Vec2::default(),
            is_kinematic: true,
        };

        world.register::<Collider>();
        world.register::<RigidBody>();
        world.register::<VelocityParent>();
        let ship = world.create_entity().with(ship_body.clone()).build();
        let platform_parent = VelocityParent { entity: ship };
        let platform = world
            .create_entity()
            .with(platform_collider.clone())
            .with(platform_body.clone())
            .with(platform_parent)
            .build();
        let move_parent = VelocityParent { entity: platform };
        let moving_entity = world
            .create_entity()
            .with(move_collider.clone())
            .with(move_body.clone())
            .with(move_parent)
            .build();

        (moving_entity, platform)
    }

    fn example_pos_parenting_setup(world: &mut World) -> (Entity, Entity) {
        let move_collider = Collider {
            global_position: Vec2::default(),
            local_position: Vec2::default(),
            lower: Vec2::new(-1., -1.),
            upper: Vec2::new(1.0, 1.0),
            opaque: true,
        };
        let platform_collider = Collider {
            local_position: Vec2::new(0.0, 2.0),
            global_position: Vec2::default(),
            lower: Vec2::new(-1., -1.),
            upper: Vec2::new(1.0, 1.0),
            opaque: true,
        };

        world.register::<Collider>();
        world.register::<PositionParent>();
        let platform = world
            .create_entity()
            .with(platform_collider.clone())
            .build();
        let move_parent = PositionParent { entity: platform };
        let moving_entity = world
            .create_entity()
            .with(move_collider.clone())
            .with(move_parent)
            .build();

        (moving_entity, platform)
    }

    #[test]
    fn velocity_parenting() {
        let (mut world, mut rtree, _pp, mut vp, mut coll, mut rigid) = collision_world();

        let (moving_entity, platform) = example_parenting_setup(&mut world);

        let mut calculate_next_frame = |world: &mut World| {
            world.write_resource::<FrameRate>().fps = 1.0;
            rtree.run_now(&world);
            vp.run_now(&world);
            coll.run_now(&world);
            rigid.run_now(&world);
            world.maintain();
        };

        calculate_next_frame(&mut world);

        let move_collider = world
            .read_storage::<Collider>()
            .get(moving_entity)
            .unwrap()
            .clone();
        assert!(move_collider.local_position.x.eq_eps(1.0, EPS));
        assert!(move_collider.local_position.y.eq_eps(0.0, EPS));

        let move_body = world
            .read_storage::<RigidBody>()
            .get(moving_entity)
            .unwrap()
            .clone();
        assert!(move_body.global_velocity.x.eq_eps(1.0, EPS));
        assert!(move_body.local_velocity.x.eq_eps(0.0, EPS));

        let platform_body = world
            .read_storage::<RigidBody>()
            .get(platform)
            .unwrap()
            .clone();
        assert!(platform_body.global_velocity.x.eq_eps(1.0, EPS));
        assert!(platform_body.local_velocity.x.eq_eps(0.5, EPS));
    }

    #[test]
    fn position_parenting() {
        let (mut world, mut rtree, mut pp, _vp, mut coll, mut rigid) = collision_world();

        let (moving_entity, platform) = example_pos_parenting_setup(&mut world);

        let mut calculate_next_frame = |world: &mut World| {
            world.write_resource::<FrameRate>().fps = 1.0;
            rtree.run_now(&world);
            coll.run_now(&world);
            rigid.run_now(&world);
            pp.run_now(&world);
            world.maintain();
        };

        calculate_next_frame(&mut world);

        let move_collider = world
            .read_storage::<Collider>()
            .get(moving_entity)
            .unwrap()
            .clone();
        assert!(move_collider.global_position.x.eq_eps(0.0, EPS));
        assert!(move_collider.global_position.y.eq_eps(2.0, EPS));

        let platform_body = world
            .read_storage::<Collider>()
            .get(platform)
            .unwrap()
            .clone();
        assert!(platform_body.global_position.x.eq_eps(0.0, EPS));
        assert!(platform_body.global_position.y.eq_eps(2.0, EPS));
    }
}
