use amethyst_core::ecs::{DispatcherBuilder, World};
use amethyst_core::{SystemBundle, SystemDesc};
use amethyst_error::Error;

use crate::impact::ImpactSystem;
use crate::parent::{PositionParentSystem, VelocityParentSystem};
use crate::system::{
    CollisionSystem, GravitySystem, RTreeBookkeepingSystemDesc, RigidBodyMovementSystem,
    TransformUpdateSystem,
};

#[derive(Default)]
pub struct BonkBundle;

impl<'a, 'b> SystemBundle<'a, 'b> for BonkBundle {
    fn build(
        self,
        world: &mut World,
        dispatcher: &mut DispatcherBuilder<'a, 'b>,
    ) -> Result<(), Error> {
        dispatcher.add(GravitySystem, "gravity_system", &[]);
        dispatcher.add_barrier();
        dispatcher.add(
            RTreeBookkeepingSystemDesc::default().build(world),
            "rtree_system",
            &[],
        );
        dispatcher.add_barrier();
        dispatcher.add(
            VelocityParentSystem::default(),
            "velocity_parent_system",
            &[],
        );
        dispatcher.add_barrier();
        dispatcher.add(CollisionSystem::default(), "collision_system", &[]);
        dispatcher.add_barrier();
        dispatcher.add(ImpactSystem::new(world), "impact_system", &[]);
        dispatcher.add_barrier();
        dispatcher.add(
            VelocityParentSystem::default(),
            "velocity_parent_system2",
            &[],
        );
        dispatcher.add_barrier();
        // Ensure the impact_system doesn't push you through stuff
        dispatcher.add(CollisionSystem::new(true), "collision_system2", &[]);
        // dispatcher.add(
        //     MovingPlatformSystem,
        //     "moving_platform_system",
        //     &["collision_system"],
        // );
        // dispatcher.add(
        //     ChildHierarchySystem,
        //     "child_hierarchy_system",
        //     &["moving_platform_system"],
        // );
        dispatcher.add_barrier();
        dispatcher.add(RigidBodyMovementSystem, "rigid_body_movement_system", &[]);
        dispatcher.add_barrier();
        dispatcher.add(
            PositionParentSystem::default(),
            "position_parent_system",
            &[],
        );
        dispatcher.add_barrier();
        dispatcher.add(TransformUpdateSystem, "transform_update_system", &[]);
        Ok(())
    }
}
