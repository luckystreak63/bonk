use amethyst_assets::PrefabData;
use amethyst_core::ecs::{Component, DenseVecStorage, Entity, FlaggedStorage, WriteStorage};
use amethyst_core::num::Zero;
use amethyst_core::Axis2;
use amethyst_derive::PrefabData;
use amethyst_error::Error;
use derivative::Derivative;
use noisy_float::prelude::*;
use rstar::{RTreeObject, AABB};
use std::ops::{Add, AddAssign, Div, Mul};
use ultraviolet::Vec2;

use serde::{Deserialize, Serialize};

pub struct FrameRate {
    pub fps: f32,
}

impl Default for FrameRate {
    fn default() -> Self {
        Self { fps: 1.0 / 60.0 }
    }
}

/// When inserted as a `Resource` into `World`,
/// affects the base strength of gravity on `RigidBody` components.
#[derive(Derivative)]
#[derivative(Default)]
pub struct Gravity {
    #[derivative(Default(value = "10.0"))]
    pub strength: f32,
}

/// Allows an entity to move through having a velocity.
///
/// A `RigidBody` also by default sets the entity to be affected by
/// the `Gravity` resource.
///
/// The reason the gravity field is a Vec2 is so that gravity can pull
/// entities in any desired direction, or none if the Vec2 is set to 0.0.
/// The gravities are multiplied, so a `gravity.y` of 0.5 with a `Gravity.strength` of
/// -10.0 will result in an effective gravity of -5.0 units per second.
#[derive(Derivative, Debug, Copy, Clone, Component, Serialize, Deserialize, PrefabData)]
#[derivative(Default)]
#[prefab(Component)]
pub struct RigidBody {
    pub local_velocity: Vec2,
    pub global_velocity: Vec2,
    #[derivative(Default(value = "Vec2::new(0.0, -1.0)"))]
    pub gravity: Vec2,
    /// Is completely unaffected by external forces like collisions
    pub is_kinematic: bool,
}

impl RigidBody {
    pub fn project_move(&self, delta_time: f32) -> Vec2 {
        self.global_velocity.mul(delta_time)
    }

    pub fn collide_with_nearest_axis(
        &mut self,
        current_collider: &RTreeCollider,
        intersections: Vec<&RTreeCollider>,
        delta_seconds: f32,
    ) -> Option<(Entity, Axis2, Vec2)> {
        let collide_x =
            current_collider.nearest_collider(self, &intersections, delta_seconds, Axis2::X);
        let collide_y =
            current_collider.nearest_collider(self, &intersections, delta_seconds, Axis2::Y);

        let new_vel = (collide_x, collide_y);
        let vel = self.global_velocity;
        match new_vel {
            (Some(x), Some(y)) => {
                if x.1.abs() < y.1.abs() {
                    self.global_velocity.x = x.1;
                    Some((x.0, Axis2::X, vel))
                } else {
                    self.global_velocity.y = y.1;
                    Some((y.0, Axis2::Y, vel))
                }
            }
            (Some(x), None) => {
                self.global_velocity.x = x.1;
                Some((x.0, Axis2::X, vel))
            }
            (None, Some(y)) => {
                self.global_velocity.y = y.1;
                Some((y.0, Axis2::Y, vel))
            }
            (None, None) => None,
        }
    }
}

fn default_opaque() -> bool {
    true
}

/// The value of `opaque` decides if the collider blocks other entities.
#[derive(Derivative, Debug, Copy, Clone, Serialize, Deserialize, PrefabData)]
#[derivative(Default)]
#[prefab(Component)]
pub struct Collider {
    #[serde(rename = "position")]
    pub local_position: Vec2,
    #[serde(skip)]
    pub global_position: Vec2,
    pub lower: Vec2,
    pub upper: Vec2,
    #[serde(default = "default_opaque")]
    pub opaque: bool,
}

impl Collider {
    pub fn new(pos: Vec2, size: Vec2, opaque: bool) -> Collider {
        Collider {
            global_position: pos,
            local_position: pos,
            lower: size.div(2.0).mul(-1.0),
            upper: size.div(2.0),
            opaque,
        }
    }
}

impl Component for Collider {
    type Storage = FlaggedStorage<Self, DenseVecStorage<Self>>;
}

#[derive(Derivative, Component)]
#[derivative(Default)]
pub struct Friction {
    #[derivative(Default(value = "512.0"))]
    pub slow_per_second: f32,
}

#[derive(Clone, Component, Debug)]
pub struct RTreeCollider {
    pub entity: Entity,
    pub opaque: bool,
    pub lower: Vec2,
    pub upper: Vec2,
}

impl PartialEq for RTreeCollider {
    fn eq(&self, other: &Self) -> bool {
        self.entity == other.entity
    }
}

impl RTreeCollider {
    pub fn new(entity: Entity, collider: &Collider) -> Self {
        RTreeCollider {
            entity,
            opaque: collider.opaque,
            lower: collider.lower.add(collider.global_position),
            upper: collider.upper.add(collider.global_position),
        }
    }

    pub(crate) fn translated(&self, amount: Vec2) -> Self {
        let mut translated = self.clone();
        translated.lower.add_assign(amount);
        translated.upper.add_assign(amount);
        translated
    }

    pub fn can_collide_with_axis(
        &self,
        other: &RTreeCollider,
        axis: usize,
        velocity: &Vec2,
    ) -> bool {
        match velocity.idx(axis) > 0.0 {
            true => self.upper.idx(axis) <= other.lower.idx(axis),
            false => self.lower.idx(axis) >= other.upper.idx(axis),
        }
    }

    pub fn collides_on_axis(&self, other: &RTreeCollider, axis: usize, velocity: &Vec2) -> bool {
        let a = axis;
        let b = 1 - a;
        velocity.idx(b).is_zero()
            || !velocity.idx(a).is_zero()
                && match velocity.idx(a) > 0.0 {
                    true => {
                        let percent_distance = Vec2::new(
                            (other.lower.x - self.upper.x) / velocity.x,
                            (other.lower.y - self.upper.y) / velocity.y,
                        );

                        (percent_distance.idx(a) <= 1.0 && percent_distance.idx(a) > 0.0)
                            && (percent_distance.idx(a) > percent_distance.idx(b)
                                || percent_distance.idx(b) <= 0.0
                                || percent_distance.idx(b) > 1.0)
                    }
                    false => {
                        let percent_distance = Vec2::new(
                            (other.upper.x - self.lower.x) / velocity.x,
                            (other.upper.y - self.lower.y) / velocity.y,
                        );

                        (percent_distance.idx(a) <= 1.0 && percent_distance.idx(a) > 0.0)
                            && (percent_distance.idx(a) > percent_distance.idx(b)
                                || percent_distance.idx(b) <= 0.0
                                || percent_distance.idx(b) > 1.0)
                    }
                }
    }

    fn distance_to_collide(&self, other: &RTreeCollider, axis: usize, velocity: &Vec2) -> f32 {
        match velocity.idx(axis) > 0.0 {
            true => other.lower.idx(axis) - self.upper.idx(axis) - 0.0001,
            false => other.upper.idx(axis) - self.lower.idx(axis) + 0.0001,
        }
    }

    fn nearest_collider(
        &self,
        rigid_body: &RigidBody,
        intersections: &Vec<&RTreeCollider>,
        delta_seconds: f32,
        axis: Axis2,
    ) -> Option<(Entity, f32)> {
        let a = axis as usize;

        let velocity = rigid_body.project_move(delta_seconds);
        let horizontal = intersections
            .iter()
            .filter(|col| self.can_collide_with_axis(col, a, &velocity))
            .filter(|col| self.collides_on_axis(col, a, &velocity))
            .min_by_key(|col| match velocity.idx(a) > 0.0 {
                true => n32(col.lower.idx(a)),
                false => n32(-col.upper.idx(a)),
            });

        let mut result = None;
        if let Some(col) = horizontal {
            let distance = self.distance_to_collide(col, a, &velocity);
            result = Some((col.entity, distance));
        }
        result
    }
}

impl RTreeObject for RTreeCollider {
    type Envelope = AABB<[f32; 2]>;

    fn envelope(&self) -> Self::Envelope {
        AABB::from_corners(*self.lower.as_array(), *self.upper.as_array())
    }
}

trait AxisIndex {
    fn idx(&self, index: usize) -> f32;
}

impl AxisIndex for Vec2 {
    fn idx(&self, index: usize) -> f32 {
        self.as_array()[index]
    }
}

// #[derive(Derivative, Debug, Copy, Clone, Component, Serialize, Deserialize, PrefabData)]
// #[derivative(Default)]
// #[prefab(Component)]
// pub struct ChildTo {
//     pub parent: Entity,
//     pub offset: Vec2,
// }

#[cfg(test)]
mod tests {
    use crate::collider::{Collider, RTreeCollider, RigidBody};
    use crate::utils::EqualsEps;
    use amethyst_core::ecs::{Builder, WorldExt};
    use amethyst_core::shred::World;
    use ultraviolet::Vec2;

    const EPS: f32 = 0.01f32;

    #[test]
    fn collision_velocity() {
        let move_collider = Collider {
            global_position: Vec2::default(),
            local_position: Vec2::default(),
            lower: Vec2::new(0., 0.),
            upper: Vec2::new(1.0, 1.0),
            opaque: false,
        };
        let mut move_body = RigidBody {
            global_velocity: Vec2::new(1.75, 1.75),
            local_velocity: Vec2::default(),
            gravity: Vec2::default(),
            is_kinematic: false,
        };

        let obstacle_1 = Collider {
            local_position: Vec2::new(1.5, 1.0),
            global_position: Vec2::new(1.5, 1.0),
            lower: Vec2::new(0., 0.),
            upper: Vec2::new(1.0, 1.0),
            opaque: true,
        };
        let obstacle_2 = Collider {
            local_position: Vec2::new(0.75, 2.0),
            global_position: Vec2::new(0.75, 2.0),
            lower: Vec2::new(0., 0.),
            upper: Vec2::new(1.0, 1.0),
            opaque: true,
        };

        let mut world = World::new();
        world.register::<Collider>();
        world.register::<RigidBody>();
        let moving_entity = world
            .create_entity()
            .with(move_collider.clone())
            .with(move_body.clone())
            .build();
        let obstacle1 = world.create_entity().with(obstacle_1.clone()).build();
        let obstacle2 = world.create_entity().with(obstacle_2.clone()).build();
        let non_refs = vec![
            RTreeCollider::new(obstacle1, &obstacle_1.clone()),
            RTreeCollider::new(obstacle2, &obstacle_2.clone()),
        ];
        let obstacles: Vec<&RTreeCollider> = non_refs.iter().collect();
        let obstacles2 = obstacles.clone();
        move_body.collide_with_nearest_axis(
            &RTreeCollider::new(moving_entity, &move_collider),
            obstacles,
            1.0,
        );
        assert!(move_body.global_velocity.x.eq_eps(0.5, EPS));
        assert!(move_body.global_velocity.y.eq_eps(1.75, EPS));
        move_body.collide_with_nearest_axis(
            &RTreeCollider::new(moving_entity, &move_collider),
            obstacles2,
            1.0,
        );
        assert!(move_body.global_velocity.x.eq_eps(0.5, EPS));
        assert!(move_body.global_velocity.y.eq_eps(1.0, EPS));
    }
}
